from backend import Backend, Cat
import discord
from logger import log

WHITE_CHECK_MARK = "\u2705"
NO_ENTRY_SIGN = "\U0001F6AB"
PREFIX = "🐱"

CMD_ADD = "$add "        # $add <cat name> (attached image) - adds a new image
CMD_DUMP = "$dump"       # $dump                            - dumps cat info [admin]
CMD_GIVE = "$give "      # $give <cat name>! (mention)      - grants ownership [admin]
CMD_IMPORT = "$import "  # $import <cat name>! (link block) - mass import of URLs [admin]
CMD_NEW = "$new "        # $new <cat name>! (mention)       - creates a cat [admin]
CMD_STATS = "$stats"     # $stats                           - shows bot statistics

HELP_USER = [
    ("Bild(er) anfordern", ":cat: Text mit **KATZENNAMEN...**"),
    ("Bild hinzufügen", ":cat: $add **KATZENNAME**\n\nBild muss als Anhang hinzugefügt werden"),
    ("Statistik zeigen", ":cat: $stats")
]
HELP_ADMIN = [
    ("Key- und ID-Dump erzeugen", ":cat: $dump"),
    ("Besitzrecht verteilen", ":cat: $give **KATZENNAME**!\n\nMarkierung benötigt"),
    ("Massenimport von URLs", ":cat: $import **KATZENNAME**!\n\nURLs zeilenweise in Codeblock angeben"),
    ("Katze erschaffen", ":cat: $new **KATZENNAME**!\n\nMarkierung des Besitzers benötigt")
]


class Client(discord.Client):
    def __init__(self, bck, bck_path, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.backend = bck
        self._backend_path = bck_path

    async def on_ready(self):
        log(f"Logged in as {self.user}")
        game = discord.Game("mit Katzen")
        await self.change_presence(status=discord.Status.online, activity=game)

    async def on_message(self, message: discord.Message):
        if message.guild is None:
            return

        for mention in message.mentions:
            if self.user.id == mention.id:
                log(f"Got mention from {message.author} on {message.guild}")
                await self.show_help(message)

        if message.content.startswith(PREFIX) and len(message.content) > 1:
            cmd = message.content[1:].strip()
            log(f"Got '{cmd}' from {message.author} on {message.guild}")
            if cmd.startswith(CMD_ADD):
                await self.handle_add(cmd[len(CMD_ADD):].strip(), message)
            elif cmd.startswith(CMD_NEW) and "!" in cmd:
                name = cmd.split("!", 1)[0][len(CMD_NEW):].strip()
                await self.handle_new_cat(name, message)
            elif cmd.startswith(CMD_GIVE) and "!" in cmd:
                name = cmd.split("!", 1)[0][len(CMD_GIVE):].strip()
                await self.handle_give(name, message)
            elif cmd.startswith(CMD_STATS):
                n = len(self.backend.cat_manager.cats)
                m = 0
                for cat in self.backend.cat_manager.cats:
                    m += len(cat.images)
                await message.channel.send(f"Ich kenne {n} Katzen und habe insgesamt {m} Katzenbilder!")
                await message.add_reaction(WHITE_CHECK_MARK)
            elif cmd.startswith(CMD_IMPORT) and "!" in cmd:
                name = cmd.split("!", 1)[0][len(CMD_IMPORT):]
                await self.handle_import(name, message)
            elif cmd.startswith(CMD_DUMP):
                if message.author.id in self.backend.admins:
                    msg = "```\n"
                    for cat_key, cat in self.backend.cat_manager.cats_by_key.items():
                        msg += f"{cat_key} = {cat.id}\n"
                    msg += "```"
                    await message.channel.send(msg)
                else:
                    await message.add_reaction(NO_ENTRY_SIGN)
            else:
                await self.handle_request(cmd, message)

    async def show_help(self, message: discord.Message):
        uid = message.author.id
        admin = uid in self.backend.admins

        embed = discord.Embed(title="Befehle", color=0xAA00AA)
        for line in HELP_USER:
            embed.add_field(name=line[0], value=line[1], inline=False)
        await message.author.send(embed=embed)
        if admin:
            embed = discord.Embed(title="Admin-Befehle", color=0x00AAAA)
            for line in HELP_ADMIN:
                embed.add_field(name=line[0], value=line[1], inline=False)
            await message.author.send(embed=embed)

        color = 0x00AA00 if admin else 0x777777
        status = WHITE_CHECK_MARK if admin else NO_ENTRY_SIGN
        cats = ", ".join(map(lambda f: f.name, self.backend.cat_manager.cats_by_owner.get(uid, [])))
        if len(cats) == 0:
            cats = "**keine**"

        embed = discord.Embed(title="Benutzer-Info", description="Hilfe wurde zugestellt :^)", color=color)
        embed.add_field(name="Admin?", value=status)
        embed.add_field(name="Eigene Katzen", value=cats)
        await message.channel.send(embed=embed)

    async def handle_add(self, cat_name: str, message: discord.Message):
        cat_name = cat_name.lower()
        key = (message.guild.id, cat_name)
        cat = self.backend.cat_manager.cats_by_key.get(key)
        if cat is None:
            await message.channel.send(f"So eine Katze kenne ich nicht :^(")
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        if message.author.id not in cat.owners:
            await message.channel.send(f"Moment... das ist nicht deine Katze! :^(")
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        if len(message.attachments) == 0:
            await message.channel.send(f"Und wo ist das Bild? :^(")
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        for entry in message.attachments:
            cat.images.append(entry.url)
        log(f"Added {len(message.attachments)} entries for {cat.name}")
        self.backend.store(self._backend_path)
        await message.add_reaction(WHITE_CHECK_MARK)

    async def handle_import(self, cat_name: str, message: discord.Message):
        if message.author.id not in self.backend.admins:
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        cat_name = cat_name.lower()
        key = (message.guild.id, cat_name)
        cat = self.backend.cat_manager.cats_by_key.get(key)
        if cat is None:
            await message.channel.send(f"So eine Katze kenne ich nicht :^(")
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        if message.author.id not in cat.owners:
            await message.channel.send(f"Moment... das ist nicht deine Katze! :^(")
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        contents = message.content
        start = contents.find("```")
        if start == -1:
            await message.channel.send(f"Kein Importblock! :^(")
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        contents = contents[(start + 3):]
        stop = contents.find("```")
        if stop == -1:
            await message.channel.send(f"Kein Importblock! :^(")
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        contents = contents[:stop].strip().replace("\r", "")
        lines = contents.split("\n")
        for line in lines:
            cat.images.append(line.strip())
        log(f"Added {len(lines)} entries for {cat.name}")
        self.backend.store(self._backend_path)
        await message.channel.send(f"{len(lines)} neue Bilder! :^)")
        await message.add_reaction(WHITE_CHECK_MARK)

    async def handle_new_cat(self, cat_name: str, message: discord.Message):
        if message.author.id not in self.backend.admins:
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        cat_name = cat_name.lower()
        key = (message.guild.id, cat_name)
        cat = self.backend.cat_manager.cats_by_key.get(key)
        if cat is not None:
            await message.channel.send(f"So eine Katze gibt es hier doch schon :^(")
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        mentions = message.mentions
        if len(mentions) != 1:
            await message.channel.send(f"Wer ist denn der Besitzer? :^(")
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        cat = Cat(self.backend.new_id(), cat_name, mentions[0].id)
        cat.guilds.append(message.guild.id)

        self.backend.cat_manager.add_cat(cat)
        self.backend.store(self._backend_path)
        log(f"New cat with name '{cat.name}' added")

        await message.channel.send("Cat added :^)")
        await message.add_reaction(WHITE_CHECK_MARK)

    async def handle_give(self, cat_name: str, message: discord.Message):
        if message.author.id not in self.backend.admins:
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        cat_name = cat_name.lower()
        key = (message.guild.id, cat_name)
        cat = self.backend.cat_manager.cats_by_key.get(key)
        if cat is None:
            await message.channel.send(f"So eine Katze gibt es hier nicht :^(")
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        mentions = message.mentions
        if len(mentions) != 1:
            await message.channel.send(f"Wer ist denn der zusätzliche Besitzer? :^(")
            await message.add_reaction(NO_ENTRY_SIGN)
            return

        cat.owners.append(mentions[0].id)
        self.backend.store(self._backend_path)
        await message.channel.send("Owner added :^)")
        await message.add_reaction(WHITE_CHECK_MARK)

    async def handle_request(self, cat_names: str, message: discord.Message):
        cat_names = cat_names.lower()

        cats = self.backend.cat_manager.cats_by_guild[message.guild.id]
        requested = set()
        while len(cat_names) > 0:
            for cat in cats:
                # TODO: sort cats by longest name first to prevent collisions "Foo" / "Foobar"
                if cat_names.startswith(cat.name):
                    requested.add(cat)
                    cat_names = cat_names[len(cat.name):]
                    break
            else:
                next_word = cat_names.find(" ")
                if next_word == -1:
                    break
                else:
                    cat_names = cat_names[(next_word + 1):]

        posted = False
        for cat in requested:
            img = cat.random_image()
            if img is None:
                await message.channel.send(f"Ich habe keine Bilder von {cat.name} :^(")
            else:
                posted = True
                await message.channel.send(img)
        if posted:
            await message.add_reaction(WHITE_CHECK_MARK)
        else:
            await message.add_reaction(NO_ENTRY_SIGN)


if __name__ == '__main__':
    with open("tokenfile", "r") as file:
        token = file.read().strip()

    path = "backend.dat"
    backend = Backend.load(path)
    client = Client(backend, path)

    client.run(token)
