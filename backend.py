from typing import Optional

from logger import log
import pickle
from random import randint

DEFAULT_ADMIN = 351638518700834817


class Backend:
    def __init__(self):
        self.admins: list[int] = [DEFAULT_ADMIN]
        self.cat_manager: CatManager = CatManager()
        self.glob_id: int = 0
        log("Created new backend")

    @staticmethod
    def load(path: str) -> "Backend":
        try:
            with open(path, "r+b") as fd:
                return pickle.load(fd)
        except FileNotFoundError:
            b = Backend()
            b.store(path)
            return b

    def store(self, path: str) -> None:
        with open(path, "w+b") as fd:
            pickle.dump(self, fd, 0)

    def new_id(self) -> int:
        res = self.glob_id
        self.glob_id += 1
        return res

    def __getstate__(self):
        return {
            "_Version": 1,
            "cat_manager": self.cat_manager,
            "admins": self.admins,
            "glob_id": self.glob_id
        }

    def __setstate__(self, state):
        if state["_Version"] >= 1:
            self.cat_manager = state["cat_manager"]
            self.admins = state["admins"]
            self.glob_id = state["glob_id"]


class CatManager:
    def __init__(self):
        self.cats: list[Cat] = []
        self.cats_by_key: dict[(int, str), Cat] = {}   # transient, key is (guild id, name)
        self.cats_by_guild: dict[int, list[Cat]] = {}  # transient, key is guild id
        self.cats_by_owner: dict[int, list[Cat]] = {}  # transient, key is user id

    def add_cat(self, cat):
        self.cats.append(cat)
        self.update_indices()

    def update_indices(self):
        self.cats_by_key = {}
        self.cats_by_guild = {}
        self.cats_by_owner = {}
        for cat in self.cats:
            for guild in cat.guilds:
                key = (guild, cat.name)
                self.cats_by_key[key] = cat

                if guild in self.cats_by_guild:
                    self.cats_by_guild[guild].append(cat)
                else:
                    self.cats_by_guild[guild] = [cat]
            for user in cat.owners:
                if user in self.cats_by_owner:
                    self.cats_by_owner[user].append(cat)
                else:
                    self.cats_by_owner[user] = [cat]

    def __getstate__(self):
        return {
            "_Version": 1,
            "cats": self.cats
        }

    def __setstate__(self, state):
        if state["_Version"] >= 1:
            self.cats = state["cats"]

        self.update_indices()
        log(f"Loaded CatManager with {len(self.cats)} cats")


class Cat:
    def __init__(self, gid: int, name: str, owner: int):
        self.id: int = gid
        self.name: str = name
        self.owners: list[int] = [owner]
        self.images: list[str] = []
        self.guilds: list[int] = []

        self._last_choice: int = -1

    def random_image(self) -> Optional[str]:
        choice = None
        maxlen = len(self.images)
        if maxlen == 0:
            return None

        while choice is None or choice == self._last_choice:
            choice = randint(0, maxlen - 1)
            if maxlen == 1:
                break  # avoid infinite loop

        self._last_choice = choice
        return self.images[choice]

    def __getstate__(self):
        return {
            "_Version": 1,
            "id": self.id,
            "name": self.name,
            "owners": self.owners,
            "images": self.images,
            "guilds": self.guilds
        }

    def __setstate__(self, state):
        if state["_Version"] >= 1:
            self.id = state["id"]
            self.name = state["name"]
            self.owners = state["owners"]
            self.images = state["images"]
            self.guilds = state["guilds"]

        self._last_choice = -1
